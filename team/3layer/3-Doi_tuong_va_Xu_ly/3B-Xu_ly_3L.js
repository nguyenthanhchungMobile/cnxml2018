//************ Xử lý Thể hiện ***************/
function Tao_Th_Danh_sach_Mat_hang(Danh_sach) {
  
  var Dia_chi_Media = "../Media"
  var Th_Danh_sach = document.createElement("div")
  Th_Danh_sach.className = "row"

  for (var i = 0; i < Danh_sach.getElementsByTagName("Mat_hang").length; i++) {
    var Mat_hang = Danh_sach.getElementsByTagName("Mat_hang")[i]
    var Ten = Mat_hang.getAttribute("Ten")
    var Ma_so = Mat_hang.getAttribute("Ma_so")
    var Don_gia_Ban = parseInt(Mat_hang.getAttribute("Don_gia_Ban"))
    var Doanh_thu = Tinh_Doanh_thu_Mat_hang(Mat_hang);  
    
    var Th_Hinh = document.createElement("img")
    Th_Hinh.src = `${Dia_chi_Media}/${Ma_so}.png`
    Th_Hinh.style.cssText = `width:150px;height:150px;`

    var Th_Thong_tin = document.createElement("div")
    Th_Thong_tin.className = `btn`
    Th_Thong_tin.style.cssText = `text-align:left`
    Th_Thong_tin.innerHTML = `${Ten}
                    <br />Đơn giá Bán: 
                    ${Don_gia_Ban.toLocaleString("vi")}
                    <br />Doanh thu:
                    ${Doanh_thu.toLocaleString("vi")}`
    var Th_Mat_hang = document.createElement("div")
    Th_Mat_hang.className = `col-md-3`
    Th_Mat_hang.style.cssText = `margin-bottom:10px`
    Th_Mat_hang.appendChild(Th_Hinh)
    Th_Mat_hang.appendChild(Th_Thong_tin)

    Th_Danh_sach.appendChild(Th_Mat_hang)
  }
  return Th_Danh_sach
}

function Tao_Th_Doanh_thu_Nha_hang(Danh_sach){
  var Th_Doanh_thu = document.createElement("div")
  var Bang_Doanh_thu = document.createElement("table")
  Bang_Doanh_thu.style.width = '100%'

  // Tạo header cho bảng
  var Dong_Tieu_de = document.createElement("tr")
  var Dong_STT = document.createElement("th")
  Dong_STT.innerHTML = "STT"
  var Dong_Ten_Mat_hang = document.createElement("th")
  Dong_Ten_Mat_hang.innerHTML = "Tên mặt hàng"
  var Dong_Ten_Loai_Mat_hang = document.createElement("th")
  Dong_Ten_Loai_Mat_hang.innerHTML = "Tên loại mặt hàng"
  var Dong_Doanh_thu = document.createElement("th")
  Dong_Doanh_thu.innerHTML = "Doanh thu"

  Dong_Tieu_de.appendChild(Dong_STT)
  Dong_Tieu_de.appendChild(Dong_Ten_Mat_hang)
  Dong_Tieu_de.appendChild(Dong_Ten_Loai_Mat_hang)
  Dong_Tieu_de.appendChild(Dong_Doanh_thu)

  Bang_Doanh_thu.appendChild(Dong_Tieu_de)

  // Nạp giá trị cho bảng
  var Danh_sach_Mat_hang = Danh_sach.getElementsByTagName("Mat_hang")
  for(var i = 0; i <Danh_sach_Mat_hang.length; i++){
    var Mat_hang = Danh_sach_Mat_hang[i]
    var Ten_Mat_hang = Mat_hang.getAttribute("Ten")
    var Ten_Loai_Mat_hang = Mat_hang.getElementsByTagName("Nhom_Mat_hang")[0].getAttribute("Ten")
    var Doanh_thu = Tinh_Doanh_thu_Mat_hang(Mat_hang)
    
    var row = document.createElement("tr")
    var td_STT = document.createElement("td");
    td_STT.innerHTML = i + 1;
    var td_Ten_Mat_hang = document.createElement("td")
    td_Ten_Mat_hang.innerHTML = Ten_Mat_hang
    var td_Ten_Loai_Mat_hang = document.createElement("td")
    td_Ten_Loai_Mat_hang.innerHTML = Ten_Loai_Mat_hang
    var td_Doanh_thu = document.createElement("td")
    td_Doanh_thu.innerHTML = Doanh_thu

    row.appendChild(td_STT)
    row.appendChild(td_Ten_Mat_hang)
    row.appendChild(td_Ten_Loai_Mat_hang)
    row.appendChild(td_Doanh_thu)

    Bang_Doanh_thu.appendChild(row)
  }

  
  var Tong_Doanh_thu = Tinh_Doanh_thu_Nha_hang(Danh_sach_Mat_hang)
  var p_Tong_Doanh_thu = document.createElement("p")
  p_Tong_Doanh_thu.innerHTML = `Tổng doanh thu: ${Tong_Doanh_thu}`
  p_Tong_Doanh_thu.style.cssText = 'font-size:20px; color: red; font-weight: bold; text-align: center'
  Th_Doanh_thu.appendChild(p_Tong_Doanh_thu)
  Th_Doanh_thu.appendChild(Bang_Doanh_thu)
  return Th_Doanh_thu
}

function Tao_Th_Tra_cuu_Mat_hang(Danh_sach_Mat_hang){
  var Dia_chi_Media = "../Media"
  var Th_Danh_sach = document.createElement("div")
  Th_Danh_sach.className = "row"

  for (var i = 0; i < Danh_sach_Mat_hang.length; i++) {
    var Mat_hang = Danh_sach_Mat_hang[i]
    var Ten = Mat_hang.getAttribute("Ten")
    var Ma_so = Mat_hang.getAttribute("Ma_so")
    var Don_gia_Ban = parseInt(Mat_hang.getAttribute("Don_gia_Ban"))
    var Doanh_thu = Tinh_Doanh_thu_Mat_hang(Mat_hang);  
    
    var Th_Hinh = document.createElement("img")
    Th_Hinh.src = `${Dia_chi_Media}/${Ma_so}.png`
    Th_Hinh.style.cssText = `width:150px;height:150px;`

    var Th_Thong_tin = document.createElement("div")
    Th_Thong_tin.className = `btn`
    Th_Thong_tin.style.cssText = `text-align:left`
    Th_Thong_tin.innerHTML = `${Ten}
                    <br />Đơn giá Bán: 
                    ${Don_gia_Ban.toLocaleString("vi")}
                    <br />Doanh thu:
                    ${Doanh_thu.toLocaleString("vi")}`
    var Th_Mat_hang = document.createElement("div")
    Th_Mat_hang.className = `col-md-3`
    Th_Mat_hang.style.cssText = `margin-bottom:10px`
    Th_Mat_hang.appendChild(Th_Hinh)
    Th_Mat_hang.appendChild(Th_Thong_tin)

    Th_Danh_sach.appendChild(Th_Mat_hang)
  }
  return Th_Danh_sach
  
}


//************** Xử lý Nghiệp vụ ***********
// Tính doanh thu mặt hàng
function Tinh_Doanh_thu_Mat_hang(Mat_hang){
    var ds_Ban_hang = Mat_hang.getElementsByTagName("Danh_sach_Ban_hang")[0].getElementsByTagName("Ban_hang")
    var Doanh_thu_Mat_hang = 0
    for(var i = 0; i<ds_Ban_hang.length; i++){     
        var Ban_hang = ds_Ban_hang[i]     
        var tien = parseInt(Ban_hang.getAttribute("Tien"))   
        Doanh_thu_Mat_hang += tien
    }
    return Doanh_thu_Mat_hang
}

// Tính doanh thu cho nhà hàng
function Tinh_Doanh_thu_Nha_hang(Danh_sach_Mat_hang){
    var Tong_Doanh_thu = 0
    for(var i = 0; i< Danh_sach_Mat_hang.length; i++){
      var Mat_hang = Danh_sach_Mat_hang[i]
      Tong_Doanh_thu += Tinh_Doanh_thu_Mat_hang(Mat_hang)
    }
    return Tong_Doanh_thu
}

// Tra cứu với Phiên bản cải tiến 
function Tim_kiem_Mat_hang(Ten_Loai_Mat_hang){
  var Danh_sach_Mat_hang = Doc_Danh_sach_Mat_hang().getElementsByTagName("Mat_hang")
  var Res_Danh_sach_Mat_hang = []
  for(var i=0; i<Danh_sach_Mat_hang.length; i++){
    var Mat_hang = Danh_sach_Mat_hang[i]
    var Ten = Mat_hang.getElementsByTagName("Nhom_Mat_hang")[0].getAttribute("Ten")
    if(Ten == Ten_Loai_Mat_hang){
      Res_Danh_sach_Mat_hang.push(Mat_hang)
    }
  }
  return Res_Danh_sach_Mat_hang
}


// ************** Xử lý Lưu trữ *********** 
// PET : Lưu ý Kỹ thuật này đã lạc lậu
// nhưng rất thích hợp khi Giảng dạy mở đầu
function Doc_Danh_sach_Mat_hang() { 
  var Xu_ly_HTTP = new XMLHttpRequest()
  Xu_ly_HTTP.open("GET", "../2-Du_lieu_Luu_tru/Du_lieu.xml", false)
  Xu_ly_HTTP.send("")
  var Chuoi_XML = Xu_ly_HTTP.responseText
  var Du_lieu = new DOMParser().parseFromString(Chuoi_XML, "text/xml").documentElement
  var Danh_sach_Mat_hang=Du_lieu.getElementsByTagName("Danh_sach_Mat_hang")[0]
  return Danh_sach_Mat_hang
}


